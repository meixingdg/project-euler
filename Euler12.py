# Project Euler Problem 12

import math

def isPrime(n):
	if n == 1:
		return false
	elif n < 4: #2 and 3 are prime
		return true
	elif n%2 == 0: #all even numbers, excluding 2, are not prime
		return false
	elif n < 9: # 4, 6, 8 already excluded
		return true
	elif n%3 == 0:
		return false
	else:
		# floor of sqrt(n)
		r = int(math.sqrt(n))
		f = 5
		while f <= r:
			#all primes greater than 3 can be written in form 6k +/- 1
			if n%f == 0: # 6k - 1
				return false
			if n%(f+2) == 0: # 6k + 1
				return false
			f = f+6 

# returns the number of divisors that the input number has
def getNumDivisors(number):
	result = 0
	#only need to check up to the floor of the square root
	for i in range(1, int(math.sqrt(number))):
		if number%i == 0:
			result += 1
	return result*2

def main():
	i = 1
	candidate = 0
	while True:
		candidate += i
		i += 1
		numDivisors = getNumDivisors(candidate)
		if numDivisors > 500:
			print candidate
			break
		

if __name__ == "__main__":
    main()