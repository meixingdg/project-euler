# Project Euler Problem 19
# How many Sundays fell on the first of the month 
#	during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

# formula derived from Gauss's algorithm for the Gregorian calendar
def getWeekday(month, day, year):
	y = year
	if month < 3:
		y = y - 1
		
	m = (month - 2)%12
	if m == 0:
		m = 12
		
	d = day
	
	weekday = (d%7 + int(2.6*m - .2)%7 + (5*(int(str(y)[2:])%4))%7 + (4*(y%100))%7 + (6*(y%400))%7)%7
	return weekday

def main():
	numSundays = 0
	day = 1
	for year in range(1901, 2001):
		for month in range(1, 13):
			weekday = getWeekday(month, day, year)
			# weekdays are numbered from 0 to 6 starting with Sunday
			if weekday == 0:
				numSundays += 1
				#print str(year) + '/' + str(month) + '/' + str(day)
	print numSundays

if __name__ == '__main__':
	main()

