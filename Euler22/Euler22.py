# Project Euler Problem 22

# ex. getNameScore("COLIN", 938) is worth
# 49714 = 53 * 938 = (3 + 15 + 12 + 9 + 14) * 938
#						^^ position of letter in alphabet
def getNameScore(name, position):
	sumName = 0
	# use the ascii values to add up name score
	for char in name:
		sumName += ord(char) - ord('A') + 1
	
	return sumName*position

def readFile(fileName):
	inputFile = open(fileName)
	nameList = inputFile.readline().split(',')
	return nameList
	
def main():
	nameList = readFile('Euler22_Input.txt')
	nameList.sort()
	
	sumScore = 0
	for i in range(0, len(nameList)):
		sumScore += getNameScore(nameList[i], i+1)
	
	print sumScore
	
if __name__ == '__main__':
	main()
		