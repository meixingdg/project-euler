# Project Euler Problem 20
# Find the sum of the digits in the number 100!

import math

def main():
	factorialStr = str(math.factorial(100))
	sum = 0
	for digitChar in factorialStr:
		sum += int(digitChar)
	print sum
	
if __name__ == "__main__":
	main()