import matplotlib

def readFile(fileName):
	inputFile = open(fileName)
	grid = []
	
	for line in inputFile:
		grid.append(line.split())
	return grid
	
def main():
	grid = readFile('Euler11_Input.txt')
	print grid
	largestProduct = 0
	
	for row in range(0, len(grid)):
		for col in range(0, len(grid[0])):
			#check horizontal adjacent numbers
			if col < 17: #stop if we have reached the end of the row
				product = 1
				for i in range(0, 4):
					product *= int(grid[row][col+i])
				if product > largestProduct:
					largestProduct = product
			
			#check vertical adjacent numbers
			if row < 17: #stop at bottom of column
				product = 1
				for i in range(0, 4):
					product *= int(grid[row+i][col])
				if product > largestProduct:
					largestProduct = product
			
			#check right diagonal adjacent numbers
			if col < 17 and row < 17:
				product = 1
				for i in range(0, 4):
					product *= int(grid[row+i][col+i])
				if product > largestProduct:
					largestProduct = product
					
			#check left diagonal adjacent numbers
			if col >= 3 and row < 17:
				product = 1
				for i in range(0, 4):
					product *= int(grid[row+i][col-i])
				if product > largestProduct:
					largestProduct = product
						
	print largestProduct
			
if __name__ == "__main__":
    main()
	

