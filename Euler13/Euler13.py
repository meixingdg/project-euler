# Project Euler Problem 13

def readFile(fileName):
	f = open(fileName)
	numbersList = []
	for line in f:
		numbersList.append(int(line))
	return numbersList
	
def main():
	numbersList = readFile("Euler13_Input.txt")
	sum = 0
	for number in numbersList:
		sum += number
		
	print str(sum)[:10]
	
if __name__ == "__main__":
	main()