# Project Euler Problem 17

normalDigits = {1:'one', 2:'two', 3:'three', 4:'four', 5:'five', 6:'six', 7:'seven', 8:'eight', 9:'nine', 0:''}
between10and20 = {10:'ten', 11:'eleven', 12:'twelve', 13:'thirteen', 14:'fourteen', \
					15:'fifteen', 16:'sixteen', 17:'seventeen', 18:'eighteen', 19:'nineteen'}
multiplesOf10 = {2:'twenty', 3:'thirty', 4:'forty', 5:'fifty', 6:'sixty', \
					7:'seventy', 8:'eighty', 9:'ninety'}
places = {3:'hundred', 4:'thousand'}

def getWordsHelper(n, place):
	nStr = str(n)
	# take care of tens and ones place
	if place < 3:
		if n < 20: 
			if n >= 10:
				result = between10and20[n]
			else: # n < 10
				result =  normalDigits[n]
		elif n >= 20:
			result = multiplesOf10[int(nStr[0])] + normalDigits[int(nStr[1])]
	# take care of hundredth and thousandth place
	elif n != 0:
		result = normalDigits[n] + places[place]
	else:
		result = ''
	return result
		
#999
def getWords(n):
	result = ''
	nStr = str(n)

	hasHundredthPlace = False
	for i in range(0, len(nStr)-2):
		hasHundredthPlace = True
		place = len(nStr) - i 
		word = getWordsHelper(int(nStr[i]), place)
		result += word
	
	# take care of last two digits, place doesn't matter here
	digits = 0
	if len(nStr) < 2:
		digits = int(nStr)
	else:
		digits = int(nStr[len(nStr)-2:])
	
	if digits != 0 and hasHundredthPlace:
		result += 'and' + getWordsHelper(digits, 1)
	else:
		result += getWordsHelper(digits, 1)
	
	return result


def main():
	letterCount = 0
	for i in range(0, 1001):
		letterCount += len(getWords(i))
		
	print letterCount
	
if __name__ == '__main__':
	main()

